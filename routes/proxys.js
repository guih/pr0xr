var express = require('express');
var request = require('request');
var router = express.Router();
var t = require('../req')
var proxy = require('../lib/proxy')
var ProxyVerifier = require('proxy-verifier');
var login = require('../lib/passport')


/* GET users listing. */
router.get('/',  function(req, res, next) {
	proxy.get(function(err, proxys){
		console.log(proxys);
		res.render('proxy', {title: 'Proxy list', users: proxys, user:req.user})		
	})
});

router.post('/add',  function(req, res, next) {

	


	var ip = req.body.ip;
	var port = req.body.port;
	var user = req.body.user;
	var pass = req.body.pass;
	var ssl = req.body.ssl;
	proxy.get(function(err, proxys){
		var t = proxys.filter(function(e){
			return e.ip == ip &&  e.port == port;
		});

		if( t.length >  0 )
			return res.status(401).json({error: 'Proxy exists!'});
		
		var proxyURL = 'http://'+user+':'+pass+'@'+ip+':'+port

		if( !user || !pass ){
			proxyURL = 'http://'+ip+':'+port
		}
		console.log(proxyURL,'<<<<<<')
	    request({
	        method: 'GET',
	        // timeout: 100,
	        headers: {
	            'User-Agent': ''
	        },        
	        strictSSL: true,
	        proxy: proxyURL,
	        uri: 'https://api.ipify.org?format=json',
	    },
	    function(err, resp, body) {
	    	console.log(arguments)
			if ( err ){
				res.status(401).json({error: 'Proxy error: '+err.code});
				return;
			}    	

			proxy.add({ip: ip, port: port, user:user, pass: pass, ssl: ssl, data: {}}, function(err, addd){
				
				// var body = JSON.parse(body);
				// // console.log(body)
				// if( body.ip != ip ){
				// 	res.status(401).json({error: 'Proxy route doesn\'t match '+ip+'!'});
				// 	return;
				// }

				if( err ){
					res.status(401).json({error: err.msg});
					return;
				}
				res.end()
			});
	    });

	})




});
router.get('/cookie/:cookie',  function(req, res, next) {
	user.find(req.params.cookie, function(err, result){

		var result = JSON.parse(result.data).cookie;
		res.json(result);
	});
});


router.post('/remove', function(req, res, next) {
	res.end()
});
router.get('/delete/:id', function(req, res, next) {
	
	var id = req.params.id;
	var password = req.body.password;

	proxy.delete(id, function(err, resp){
		if(err){
			console.log(err);
			res.redirect('/proxys');

			return
		}

		res.redirect('/proxys');
	})

});


module.exports = router;


