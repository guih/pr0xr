var express = require('express');
var request = require('request');
var router = express.Router();
var t = require('../qq')

var user = require('../lib/users')
var statics  = require('../lib/static');

var ua = statics.UA[Math.floor(Math.random() * statics.UA.length)];

var login = require('../lib/passport')

/* GET users listing. */
router.get('/', function(req, res, next) {
	user.get(function(err, result){
		res.render('users', {title: 'Users', users: result, user:req.user})		
	})
});
router.get('/navigate/:id',  function(req, res, next) {
	user.find(req.params.id, function(err, result){
		var password = JSON.parse(result.data).password;

		t.adv(result.email, password, true, function(e){	
			res.send(e.markup)
		});
	});
});

// router.post('/add', login.ensureAuthenticated, function(req, res, next) {
router.post('/add',  function(req, res, next) {

	// console.log(req.body)
	var email = req.body.email;
	var password = req.body.password;
	t.login(email, password, function(err, e){	    

		console.log(err)
		if( err ){
			res.status(401).json(e);
			return;
		}
		// e.info.password = password;
		user.add({name: e.NAME, email: email, pass: password, data:e}, function(err, addd){
			if( err ){
				res.status(401).json({error: err.msg});
				return;
			}
			res.end()
		});
	})

});

router.get('/delete/:id', function(req, res, next) {
	
	var id = req.params.id;
	var password = req.body.password;

	user.delete(id, function(err, resp){
		if(err){
			console.log(err);
			res.redirect('/users');

			return
		}

		res.redirect('/users');
	})

});

router.get('/used/:id', function(req, res, next) {
	
	var id = req.params.id;
	var password = req.body.password;

	user.edit(id, function(err, resp){
		if(err){
			console.log(err);
			res.redirect('/users');
			return
		}

		res.redirect('/users');
	})

});
router.get('/cookie/:cookie', function(req, res, next) {
	user.find(req.params.cookie, function(err, result){

		var result = JSON.parse(result.data.jar);
		result = result._jar.cookies;
		result = result.map(function(y){ y.name = y.key; delete y.key; return y; });
		res.json(result);
	});
});

router.post('/remove', function(req, res, next) {
	t.login('tyleryoussef@linuxmail.org', 'face4321', function(e){
		console.log(e.info)

	    res.end('<div class="status success">Conectado - IP: 192.168.0.1</div>'+e.markup);
	})
});

module.exports = router;


// (Five1TwoFour^Eight0Seven)+""+(OneOneFourSeven^Three4Zero)+""+(SixFiveEightOne^Three3Six)+""+(Nine4OneSix^OneZeroThree)
