
var mongoose = require('mongoose');
var db   = require('../models/db');
var User     = require('../models/User');


exports.get = function(fn){
	User.find({}, function (err, users) {
		if( !users){
			fn({
				error: true,
				msg: "Can't find users"
			});
			return;
		}

		fn(null, users);
	});		
}
exports.find = function(id, fn){
	User.find({ '_id': id }, function (err, users) {
		if( !users){
			fn({
				error: true,
				msg: "Can't find user with this id."
			}, null);
			return;
		}

		fn(null, users[0]);
	});	
}

exports.getUser = function(email, fn){
	console.log('???????', { 'email': email })
	User.find({ 'email': email }, function (err, users) {
		console.log('??????? ERR', err)
		if( !users){
			fn({
				error: true,
				msg: "Can't find user with this name."
			}, null);
			return;
		}

		fn(null, users[0]);
	});	
}

exports.edit = function(id, fn){
	User.find({ _id: id }, function(err, users) {
		user = users[0];
		user.data.name.IS_DEACTIVATED_ALLOWED_ON_MESSENGER = true;

		console.log(user)
		user.save(function(saveErr, savedContact) {
			console.log('done')
			if (saveErr) throw saveErr;
			fn(null, savedContact);
		});
	})	
}

exports.delete = function(id, fn){
	User.remove({ _id: id }, function(err) {
		if (!err) {
			fn({err: 'Error'})
			return
		}
	
		fn(null, {})
	});
}
exports.add = function(userAccount, fn){
			// console.log('TRY QUERY!', userAccount)

	this.getUser( userAccount.email, function(err, users){
			// console.log('QUERYED!', userAccount)

		if( !err && users ){
			fn({
				error: true,
				msg: "User exists"
			}, null);	
			return;
		}
		// userAccount.data  = JSON.stringify(userAccount.data);

		User.create(userAccount, function(err, savedUser){
			if( err ){
				fn({
					error: true,
					msg: "Error while adding user."
				}, null);
				return;
			}


			console.log('Success!')
			fn(null, savedUser);
		});	
		
	} )
}
