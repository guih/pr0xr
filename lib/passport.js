var express                 = require("express");
var app                     = express();
var passport                = require('passport');
var GoogleStrategy          = require('passport-google-oauth').OAuth2Strategy;


var staticVars              = require('../lib/static');
var account                 = require('../lib/SysUser');



passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(obj, done) {
  done(null, obj);
});

exports.auth = passport.authenticate('google', { 
    scope: staticVars.CLIENT_SCOPE
});

exports.cb = passport.authenticate('google', { 
    failureRedirect: '#/login' 
});

exports.strategy = new GoogleStrategy({
        clientID        : staticVars.CLIENT_ID,
        clientSecret    : staticVars.CLIENT_SECRET,
        callbackURL     : staticVars.CALLBACK_URL
    },
    function(req, accessToken, refreshToken, profile, done) {
        process.nextTick(function () {
            var user = profile;

            var userMeta = user._json;

            
            if( userMeta.emails[0].value.indexOf("gmail.com") == -1 ){
                return done( null, {
                    error: "<i class=\"material-icons\">info_outline</i> Your email domain don't match any of allowed ones."
                });
            }

            account.getUser( userMeta.id, function(err, user){
            
                if( err ){
                    console.log(err);
                    // return;
                }
                console.log(user, userMeta.id, user);
                // return;
                if( !user ){
                    account.insertUser( userMeta, function(err, createdUser){
                        return done( null, {
                            error: "<i class=\"material-icons\">info_outline</i> Your account is created but not activated, get in touch with administrator in order to allow your access."
                        });
                    } );                
                }else{
                    if( user.verified == false ){
                        return done( null, {
                            error: "<i class=\"material-icons\">info_outline</i> You must wait audition before you have access to the system."
                        });
                    }else{
                        return done( null, user );                        
                    }

                }
            } )
        });
    }
);
exports.ensureAuthenticated = function(req, res, next) {


  if (req.isAuthenticated() && !req.user.error) { return next(); }
  res.redirect('/auth');
}
