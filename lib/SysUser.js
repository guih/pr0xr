var mongoose = require('mongoose');
var db   = require('../models/db');
var User     = require('../models/SysUser');
var user 	= mongoose.model('SysUser');


exports.get = function(fn){
	user.find({}, function (err, users) {
		if( !users){
			fn({
				error: true,
				msg: "Can't find users"
			});
			return;
		}

		fn(null, users);
	});	
}
exports.getUser = function(id, fn){
	user.find({ 'id': id }, function (err, users) {
		if( !users){
			fn({
				error: true,
				msg: "Can't find user with this name."
			}, null);
			return;
		}

		fn(null, users[0]);
	});	
}
exports.insertUser = function(userAccount, fn){
	user.create(userAccount, function(err, savedReport){
		if( err ){
			fn({
				error: true,
				msg: "Error while adding user."
			}, null);
			return;
		}
		fn(null, savedReport);
	});	
}
exports.checkUser = function(){
	
}