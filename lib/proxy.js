
var mongoose = require('mongoose');
var db   = require('../models/db');
var Proxy     = require('../models/Proxy');


exports.get = function(fn){
	Proxy.find({}, function (err, proxys) {
		if( !proxys){
			fn({
				error: true,
				msg: "Can't find proxys"
			});
			return;
		}

		fn(null, proxys);
	});		
}
exports.find = function(id, fn){
	Proxy.find({ '_id': id }, function (err, proxys) {
		if( !proxys){
			fn({
				error: true,
				msg: "Can't find proxy with this id."
			}, null);
			return;
		}

		fn(null, proxys[0]);
	});	
}

exports.getProxy = function(query, fn){
	Proxy.find(query, function (err, proxys) {
		if( !proxys){
			fn({
				error: true,
				msg: "Can't find proxy with this name."
			}, null);
			return;
		}

		fn(null, proxys[0]);
	});	
}

exports.delete = function(id, fn){
	Proxy.remove({ _id: id }, function(err) {
		if (!err) {
			fn({err: 'Error'})
			return
		}
	
		fn(null, {})
	});
}

exports.add = function(proxyData, fn){
	this.getProxy( { ip: proxyData.ip, port: proxyData.port}, function(err, proxys){
		// if( !err && proxys )
		// 	return fn({
		// 		error: true,
		// 		msg: "proxy exists"
		// 	}, null);	

	

		Proxy.create(proxyData, function(err, savedProxy){
		
			if( err )
				return fn({
					error: true,
					msg: "Error while adding proxy."
				}, null);
		


			fn(null, savedProxy);
		});	
		
	} )
}
