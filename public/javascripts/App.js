;(function($){
	var App = {};


	App.init = function(){
		this.initListeners();
	};

	App.initListeners = function(){
		$('.add-new').on('click', function(){
			if( $('.add-user').is(':visible') ){
				$('.add-user').hide();
				return;
			}

			$('.add-user').fadeIn();

		});
		$('#driver-used').bind('keyup', function() {
		    var s = new RegExp(this.value.toLowerCase());
		    $('.uu.yeld-table tr').each(function() {
		        if(s.test(this.innerHTML.toLowerCase())) $(this).show();
		        else $(this).hide();
		    });
		});
		$('#driver').bind('keyup', function() {
		    var s = new RegExp(this.value.toLowerCase());
		    $('.u.yeld-table tr').each(function() {
		        if(s.test(this.innerHTML.toLowerCase())) $(this).show();
		        else $(this).hide();
		    });
		});
		$('.push-proxy').on('click', function(){

			var $form = $('#proxy-add-new').serialize();
			var $formArray = $('#proxy-add-new').serializeArray();

			if( !$formArray[0].value || $formArray[0].value == "" )
				return;
			
			if( !$formArray[1].value || $formArray[1].value == "" )
				return;
			
			$('.wait').removeClass('success').removeClass('error');
			$('.wait').html('<img src="images/ajax-load.gif"> Please wait...');
			$('.yeld-form').hide();
			$('.wait').fadeIn();

			$.post('/proxys/add', $form).done(function(res){				
				$('.wait').addClass('success').html('<i class="material-icons">check</i> Success!');
				setTimeout(function(){
					window.location.reload();					
				},1000)
			}).fail(function(res){
				var res = JSON.parse( res.responseText );
		
				$('.wait').addClass('error').html('<i class="material-icons">error_outline</i> '+res.error);
				$('.yeld-form').fadeIn();
				return;
			});
		});

		$('.unverify').on('click', function(){
	

			$.post('/users/unverify', { _id: $(this).data('id') }).done(function(res){
				setTimeout(function(){
					window.location.reload();					
				},1000)
			}).fail(function(res){
				var res = JSON.parse( res.responseText );
		
				$('a.btn').addClass('error').html('<i class="material-icons">error_outline</i> '+res.error);
				$('a.btn').html('<i class="material-icons">error_outline</i> '+res.error);
				return;
			});			
		});
		$('.verify').on('click', function(){
	

			$.post('/users/verify', { _id: $(this).data('id') }).done(function(res){
				setTimeout(function(){
					window.location.reload();					
				},1000)
			}).fail(function(res){
				var res = JSON.parse( res.responseText );
		
				$('a.btn').addClass('error').html('<i class="material-icons">error_outline</i> '+res.error);
				$('a.btn').html('<i class="material-icons">error_outline</i> '+res.error);
				return;
			});			
		});


		$('.save-settings').on('click', function(){
			var $form = $('#settings').serialize();

			$.post('/config', $form).done(function(res){
				$('a.btn').addClass('success').html('<i class="material-icons">check</i> Success!');
				setTimeout(function(){
					window.location.reload();					
				},1000)
			}).fail(function(res){
				var res = JSON.parse( res.responseText );
		
				$('a.btn').addClass('error').html('<i class="material-icons">error_outline</i> '+res.error);
				$('a.btn').html('<i class="material-icons">error_outline</i> '+res.error);
				return;
			});			
		});

		$('.show-password').on('click', function(){
			var $sl = $(this).parent().find('span');
			
			console.log($sl);
			if( !$sl.hasClass('show') ){
				$sl.addClass('show');
				$(this).text('Hide');	
				return;
			}

			$sl.removeClass('show');
			$(this).text('Show');	


		});

		$('.push-user').on('click', function(){

			var $form = $('#user-add-new').serialize();
			var $formArray = $('#user-add-new').serializeArray();

			if( !$formArray[0].value || $formArray[0].value == "" )
				return;
			
			if( !$formArray[1].value || $formArray[1].value == "" )
				return;
			
			$('.wait').removeClass('success').removeClass('error');
			$('.wait').html('<img src="images/ajax-load.gif"> Please wait...');
			$('.yeld-form').hide();
			$('.yeld-form').hide();
			$('.wait').fadeIn();

			$.post('/users/add', $form).done(function(res){
				$('.wait').addClass('success').html('<i class="material-icons">check</i> Success!');
				setTimeout(function(){
					window.location.reload();					
				},1000)
			}).fail(function(res){
				var res = JSON.parse( res.responseText );
		
				$('.wait').addClass('error').html('<i class="material-icons">error_outline</i> '+res.error);
				$('.wait').html('<i class="material-icons">error_outline</i> '+res.error);
				$('.yeld-form').fadeIn();
				return;
			});
		});
	}

	App.init();
})(jQuery);
