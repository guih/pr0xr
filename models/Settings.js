var mongoose = require('mongoose');  
var Settings = new mongoose
.Schema({ 
  'private': Object,
  app: { type: Object, default : {
  	appName: 'Pr0xr', 
  	adminEmail: '',
  	redirectUrl: '',
  	requestsTimeout: ''
  } },
  'version': { type: String, default :'0.0.1-alpha-genesis'},
});

module.exports = mongoose.model('Settings', Settings);


