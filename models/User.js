var mongoose = require('mongoose');  
var user = new mongoose.Schema({  
	name: String,
	email: String,
	pass: String,
	data: Object,
});
module.exports = mongoose.model('User', user);