var mongoose = require('mongoose');  
var Proxy = new mongoose
.Schema({  
	ip: String,
	port: String,
	user: String,
	pass: String,
	used: { type: Number, default: 0 },
	ssl: { type: Boolean, default: false },
	data: Object
});
module.exports = mongoose.model('Proxy', Proxy);