var mongoose = require('mongoose');  
var SysUser = new mongoose.Schema( { 
	kind: String,
	etag: String,
	emails: Object,
	objectType: String,
	id: String,
	displayName: String,
	name: { type: Object, default: { familyName: 'N/A', givenName: 'User' } },
	url: String,
	image: { 
		type: Object,
		default: { 
			url: 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50',
			isDefault: true 
		} 
	},
	isPlusUser: Boolean,
	language: String,
	circledByCount: Number,
	verified: Boolean ,
	level: Number,
});
module.exports = mongoose.model('SysUser', SysUser);