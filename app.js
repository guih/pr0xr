var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var settings = require('./lib/settings');
var login = require('./lib/passport');
var session = require('express-session')


settings.get(function(err, result){
	console.log(arguments, 'settings')
	if( err || result.length == 0 ){
		settings.add(function(){
			console.log('settings sadded!')
		});
	}else{
		console.log('settings already exisrts!')
	}
})


var index = require('./routes/index');
var users = require('./routes/users');
var proxy = require('./routes/proxys');

var app = express();
var passport = require('passport'),    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override') //used to manipulate POST
  , util = require('util')
  , GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(session({
  secret: 'keyboard cat'
}));
passport.use(login.strategy);

app.use(passport.initialize());

app.use(passport.session());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);
app.use('/proxys', proxy);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
